<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Environment;

/**
 * Paging class which extracts any notion of calculation and data retrieval from our controllers
 */
class Pagination
{
    /**
     * The name of the entity on which we want to perform a pagination
     *
     * @var string
     */
    private $entityClass;

    /**
     * @var integer
     */
    private $limit = 10;

    /**
     * @var integer
     */
    private $currentPage = 1;

    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var Twig\Environment
     */
    private $twig;

    /**
     * The name of the road that you want to use for the navigation buttons
     *
     * @var string
     */
    private $route;

    /**
     * The path to the template that contains the pagination
     *
     * @var string
     */
    private $templatePath;

    /**
     * @param ObjectManager $manager
     * @param Environment $twig
     * @param RequestStack $request
     * @param string $templatePath
     */
    public function __construct(EntityManagerInterface $em, Environment $twig, RequestStack $request, $templatePath)
    {
        $this->em = $em;
        $this->twig = $twig;
        $this->templatePath = $templatePath;
        $this->route = $request->getCurrentRequest()->attributes->get('_route');
    }


    /**
     * Allows you to display the navigation rendering within a template branch!
     *
     * We use here our rendering engine to compile the template which is at the path
     * of our $templatePath property, passing it the variables:
     * - page  => the current page we are on
     * - pages => the total number of pages that exist
     * - route => the name of the route to use for the navigation links
     *
     * Attention : it directly displays the rendering
     *
     * @return void
     */
    public function display()
    {
        $this->twig->display($this->templatePath, [
            'page' => $this->currentPage,
            'pages' => $this->getPages(),
            'route' => $this->route
        ]);
    }

    /**
     * Allows you to retrieve the number of pages that exist on a particular entity
     *
     * @throws Exception if the $entityClass property is not configured
     *
     * @return int
     */
    public function getPages(): int
    {
        if (empty($this->entityClass)) {
            // If there is no entity configured, we cannot load the repository !
            throw new \Exception("You did not specify the entity on which we must paginate! Use the setEntityClass() method of your PaginationService!");
        }

        $total = count($this->em
            ->getRepository($this->entityClass)
            ->findAll());

        return ceil($total / $this->limit);
    }

    /**
     * Allows you to retrieve paginated data for a specific entity
     *
     * @throws Exception if the $entityClass property is not set
     *
     * @return array
     */
    public function getData()
    {
        if (empty($this->entityClass)) {
            throw new \Exception("Vous n'avez pas spécifié l'entité sur laquelle nous devons paginer ! Utilisez la méthode setEntityClass() de votre objet PaginationService !");
        }

        $offset = $this->currentPage * $this->limit - $this->limit;

        return $this->em
            ->getRepository($this->entityClass)
            ->findBy([], [], $this->limit, $offset);
    }

    /**
     * @param int $page
     * @return self
     */
    public function setPage(int $page): self
    {
        $this->currentPage = $page;

        return $this;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->currentPage;
    }

    /**
     * @param int $limit
     * @return self
     */
    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * Allows you to specify the entity on which you want to paginate
     * exemple :
     * - App\Entity\Ad::class
     * - App\Entity\Comment::class
     *
     * @param string $entityClass
     * @return self
     */
    public function setEntityClass(string $entityClass): self
    {
        $this->entityClass = $entityClass;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntityClass(): string
    {
        return $this->entityClass;
    }

    /**
     * Allows you to choose a pagination template
     *
     * @param string $templatePath
     * @return self
     */
    public function setTemplatePath(string $templatePath): self
    {
        $this->templatePath = $templatePath;

        return $this;
    }

    /**
     * @return string
     */
    public function getTemplatePath(): string
    {
        return $this->templatePath;
    }

    /**
     * Allows you to change the default route for navigation links
     *
     * @param string $route
     * @return self
     */
    public function setRoute(string $route): self
    {
        $this->route = $route;

        return $this;
    }

    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }
}
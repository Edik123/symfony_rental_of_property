<?php

namespace App\Controller;

use App\Entity\Ad;
use App\Form\AdType;
use App\Entity\Image;
use App\Repository\AdRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/ads", name="ads_index")
     */
    public function index(AdRepository $rep)
    {
        $ads = $rep->findAll();

        return $this->render('ad/index.html.twig', [
            'ads' => $ads,
        ]);
    }

     /**
     * @Route("/ads/create", name="ads_create")
     * @IsGranted("ROLE_USER")
     * 
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $ad = new Ad();

        $image = new Image();
        $image->setUrl('http://placehold.it/400x200')
              ->setCaption('title');

        $ad->addImage($image);

        $form = $this->createForm(AdType::class, $ad);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($ad->getImages() as $image)
            {
                $image->setAd($ad);
                $this->em->persist($image);
            }
            
            $ad->setAuthor($this->getUser());

            $this->em->persist($ad);
            $this->em->flush();

            $this->addFlash(
                'success',
                "Announcement <b>{$ad->getTitle()}</b> was registered!"
            );

            $this->redirectToRoute('ads_show', [
                'slug' => $ad->getSlug()
            ]);
        }

        return $this->render('ad/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

     /**
     * @Route("/ads/{slug}/edit", name="ads_edit")
     * @Security("is_granted('ROLE_USER') and user === ad.getAuthor()", message="This ad does not belong to you, you cannot modify it")
     * 
     * @param Ad $ad
     * @param Request $request
     * @return Response
     */
    public function edit(Ad $ad, Request $request)
    {
        $form = $this->createForm(AdType::class, $ad);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            foreach ($ad->getImages() as $image)
            {
                $image->setAd($ad);
                $this->em->persist($image);
            }

            $this->em->persist($ad);
            $this->em->flush();

            $this->addFlash(
                'success',
                "Changes to the ad <b>{$ad->getTitle()}</b> have been registered!"
            );

            return $this->redirectToRoute('ads_show', [
                'slug' => $ad->getSlug()
            ]);
        }

        return$this->render('ad/edit.html.twig', [
            'form' => $form->createView(),
            'ad' => $ad
        ]);
    }

    /**
     * @Route("/ads/{slug}/delete", name="ads_delete")
     * @Security("is_granted('ROLE_USER') and user == ad.getAuthor()", message="You do not have the right to access this resource")
     * 
     * @param Ad $ad
     * @return Response
     */
    public function delete(Ad $ad){
        $this->em->remove($ad);
        $this->em->flush();

        $this->addFlash(
            'success',
            "The announcement <strong>{ $ad->getTitle() }</strong> has been deleted!"
        );

        return $this->redirectToRoute("ads_index");
    }

    /**
     * Slug parameter conversion via ParamConverter to ad
     * 
     * @Route("/ads/{slug}", name="ads_show")
     * 
     * @param Ad $ad
     * @return Response
     */
    public function show(Ad $ad)
    {
        return $this->render('ad/show.html.twig', [
               'ad' => $ad
        ]);
    }
}

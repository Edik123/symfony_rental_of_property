<?php

namespace App\Controller\Admin;

use App\Entity\Ad;
use App\Form\AdType;
use App\Service\Pagination;
use App\Repository\AdRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/admin/ads/{page<\d+>?1}", name="admin_ads_index")
     */
    public function index(AdRepository $repo, $page, Pagination $pagination): Response
    {
        $pagination->setEntityClass(Ad::class)
                    ->setPage($page);
        
        return $this->render('admin/ad/index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * Allows you to display the edit form
     * 
     * @Route("/admin/ads/{id}/edit", name="admin_ads_edit")
     * 
     * @param Ad $ad
     * @return Response
     */
    public function edit(Ad $ad, Request $request)
    {
        $form =$this->createForm(AdType::class, $ad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) 
        {
            $this->em->persist($ad);
            $this->em->flush();

            $this->addFlash(
                'success',
                "The announcement <strong>{$ad->getTitle()}</strong> has been registered!"
            );
        }

        return $this->render('admin/ad/edit.html.twig', [
            'ad' => $ad,
            'form' => $form->createView()
        ]);
    }

    /**
     * Allows you to delete an ad
     * 
     * @Route("/admin/ads/{id}/delete", name="admin_ads_delete")
     * 
     * @param Ad $ad
     * @return Response
     */
    public function delete(Ad $ad)
    {
        if (count($ad->getBookings()) > 0){
            $this->addFlash(
                'warning',
                "You cannot delete the ad <strong>{$ad->getTitle()}</strong> because she already has reservations!"
            );
        } else {
            $this->em->remove($ad);
            $this->em->flush();

            $this->addFlash(
                'success',
                "The announcement <strong>{$ad->getTitle()}</strong> has been deleted!"
            );
        }
        return $this->redirectToRoute('admin_ads_index');
    }
}

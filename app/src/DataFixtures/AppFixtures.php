<?php

namespace App\DataFixtures;

use App\Entity\Ad;
use App\Entity\Role;
use App\Entity\User;
use App\Entity\Image;
use App\Entity\Booking;
use App\Entity\Comment;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private const GENDERS = ['male', 'female'];

    /**
     * @var ObjectManager $manager
     */
    private $em;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

     /**
     * @var \Faker\Factory
     */
    private $faker;

    /**
     * @var User
     */
    private $users;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->faker = \Faker\Factory::create();
    }
    
    public function load(ObjectManager $manager)
    {
        $this->loadUsers($manager);
        $this->loadAds($manager);
        $manager->flush();
    }

    public function loadUsers(ObjectManager $manager)
    {
        $this->users = [];

        $adminRole = new Role();
        $adminRole->setTitle('ROLE_ADMIN');

        $admin = new User();
        $admin->setFirstName("Ed")
                ->setLastName("Vax")
                ->setEmail("ed_va@mail.ru")
                ->setHash($this->passwordEncoder->encodePassword($admin, "password"))
                ->setAvatar("https://assets.zyrosite.com//AEz1zjxJ4qH2aDXv/photo-mp01M5yQMMcRoXyg.jpg")
                ->setIntroduction($this->faker->sentence())
                ->setDescription('<p>'.join('</p><p>', $this->faker->paragraphs(3)).'</p>')
                ->addUserRole($adminRole);
        
        $manager->persist($adminRole);
        $manager->persist($admin);

        for ($i = 1; $i <= 10; $i++) 
        {
            $user = new User();

            $gender = $this->faker->randomElement(SELF::GENDERS);

            $picture = 'https://randomuser.me/api/portraits/';
            $pictureId = $this->faker->numberBetween(1, 99) . '.jpg';

            $picture .= ($gender == 'male' ? 'men/' : 'women/') . $pictureId;

            $hash = $this->passwordEncoder->encodePassword($user, 'password');

            $user->setFirstName($this->faker->firstName)
                 ->setLastName($this->faker->lastName)
                 ->setEmail($this->faker->email)
                 ->setIntroduction($this->faker->sentence())
                 ->setDescription('<p>' . join('</p><p>', $this->faker->paragraphs(3)) . '</p>')
                 ->setHash($hash)
                 ->setAvatar($picture);

            $this->users[] = $user;
            $manager->persist($user);
        }
    }

    public function loadAds(ObjectManager $manager)
    {
        for ($i = 1; $i <= 30; $i++)
        {
            $ad = new Ad();

            $author = $this->users[mt_rand(0, count($this->users) - 1)];

            $ad->setTitle($this->faker->sentence())
                ->setCoverImage($this->faker->imageUrl(1000,350))
                ->setIntroduction($this->faker->paragraph(2))
                ->setContent('<p>'. join('<p></p>', $this->faker->paragraphs(5)) . '</p>')
                ->setPrice(mt_rand(40,200))
                ->setRooms(mt_rand(1,5))
                ->setAuthor($author);
            
            for ($j = 1; $j <= mt_rand(2,5); $j++)
            {
                $image = new Image();

                $image->setUrl($this->faker->imageUrl())
                    ->setCaption($this->faker->sentence())
                    ->setAd($ad);

                $manager->persist($image);
            }
            
            $this->loadBookings($ad, $manager);

            $manager->persist($ad);
        }
    }

    public function loadBookings(Ad $ad, ObjectManager $manager)
    {
        // Reservations management
        for ($j = 1; $j <= mt_rand(0, 10); $j++) {
            $booking = new Booking();

            $createdAt = $this->faker->dateTimeBetween('-6 months');
            $startDate = $this->faker->dateTimeBetween('-3 months');
            
            $duration = mt_rand(3, 10);
            $endDate = (clone $startDate)->modify("+$duration days");

            $comment = $this->faker->paragraph();
            $amount = $ad->getPrice() + $duration;
            $booker = $this->users[mt_rand(0, count($this->users) - 1)];

            $booking->setBooker($booker)
                ->setAd($ad)
                ->setStartDate($startDate)
                ->setEndDate($endDate)
                ->setCreateAt($createdAt)
                ->setAmount($amount)
                ->setComment($comment);

            $manager->persist($booking);

            // comment management
            if(mt_rand(0,1)){
                $comment = $this->loadComment($ad, $booker);
                $manager->persist($comment);
            }
        }
    }

    public function loadComment(Ad $ad, $booker)
    {
        $comment = new Comment();
        $comment
            ->setContent($this->faker->paragraph())
            ->setRating(mt_rand(1,5))
            ->setAuthor($booker)
            ->setAd($ad);
        
        return $comment;
    }
}

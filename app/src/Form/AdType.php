<?php

namespace App\Form;

use App\Entity\Ad;
use App\Form\ImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class AdType extends AbstractType
{
    use ConfigurationTrait;
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, $this->getConfiguration("Title", "Enter a title for your ad"))
            ->add('slug', TextType::class, $this->getConfiguration("Web address","Enter web address",['required' => false]))
            ->add('price', MoneyType::class, $this->getConfiguration("Price per night", "Specify your desired price for one night"))
            ->add('introduction', TextType::class, $this->getConfiguration("Introduction", "Give a general description of your ad"))
            ->add('content', TextareaType::class, $this->getConfiguration("Detailed description", "Enter a full description"))
            ->add('coverImage', UrlType::class, $this->getConfiguration("Image url", "Enter the URL of the image"))
            ->add('rooms', IntegerType::class, $this->getConfiguration("Number of rooms", "Number of free rooms"))
            ->add('images', CollectionType::class,
                [
                    'entry_type' => ImageType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    'attr' => [
                        'class' => 'my-selector',
                    ],
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ad::class,
        ]);
    }
}

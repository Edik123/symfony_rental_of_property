<?php

namespace App\Form;

use App\Entity\Booking;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class BookingType extends AbstractType
{
    use ConfigurationTrait;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate', DateType::class, ["widget" => "single_text"], $this->getConfiguration("Arrival date "," The date you expect to arrive"))
            ->add('endDate', DateType::class, ["widget" => "single_text"], $this->getConfiguration("Date of departure "," Date on which you leave the premises"))
            ->add('comment', TextareaType::class, $this->getConfiguration(false, "If you have a comment, do not hesitate to share it!", ["required"=>false]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Booking::class,
        ]);
    }
}

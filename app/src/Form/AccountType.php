<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class AccountType extends AbstractType
{
    use ConfigurationTrait;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, $this->getConfiguration("First name", "Your first name"))
            ->add('lastName', TextType::class, $this->getConfiguration("Last name", "Your last name"))
            ->add('email', EmailType::class, $this->getConfiguration("Email", "Your email address"))
            ->add('avatar', UrlType::class, $this->getConfiguration("Avatar", "URL of your avatar"))
            ->add('introduction', TextType::class, $this->getConfiguration("Introduction", "Introduce yourself in a few words ..."))
            ->add('description', TextareaType::class, $this->getConfiguration("Detailed description", "It's time to introduce yourself in detail!"))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
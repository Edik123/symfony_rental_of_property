<?php

namespace App\Form;

trait ConfigurationTrait
{
/**
     * @param string $label
     * @param string $placeholder
     * @param array  $options
     *
     * @return array
     */
    public function getConfiguration($label, $placeholder, $options = [])
    {
        return array_merge_recursive([
            'label' => $label,
            'attr' => [
                'placeholder' => $placeholder
            ]
        ], $options);
    }
}
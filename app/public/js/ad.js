$('#add-image').click(function(){
    // getting the number of future fields that I will create
    const index = +$('#widgets-counter').val();
    console.log(index);
    // getting a prototype record
    const tmpl = $('#ad_images').data('prototype').replace(/__name__/g, index);

    // paste this code into div
    $('#ad_images').append(tmpl);

    $('#widgets-counter').val(index + 1);

    handleDeleteButtons();
});

function handleDeleteButtons() {
    $('button[data-action="delete"]').click(function(){
        const target = this.dataset.target;
        $(target).remove();
    });
}

function updateCounter() {
    const count = +$('#ad_images div.form-group').length;

    $('#widgets-counter').val(count);
}

updateCounter();
handleDeleteButtons();
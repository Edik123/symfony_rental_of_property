up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear docker-build docker-up app-init

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

app-cli:
	docker-compose run --rm app-php-cli php bin/console

app-init: app-composer-install app-createdb app-migrations app-fixtures

app-wait-db:
	until docker-compose exec -T database pg_isready --timeout=0 --dbname=app ; do sleep 1 ; done

app-composer-install:
	docker-compose run --rm app-php-cli composer install

app-migrations:
	docker-compose run --rm app-php-cli php bin/console doctrine:migrations:migrate --no-interaction

app-fixtures:
	docker-compose run --rm app-php-cli php bin/console doctrine:fixtures:load --no-interaction

app-createdb:
	docker-compose run --rm app-php-cli php bin/console doctrine:database:create
